---
- name: Install Python snipeit module
  pip:
    name: snipeit

# Installation tasks
####################
- name: Apply general Ansible master role
  ansible.builtin.include_role:
    name: ethz.system_management.ansible_master

- name: "Install ansible collections (not forcing updates)"
  ansible.builtin.command:
    argv:
      - /usr/local/bin/ansible-galaxy
      - collection
      - install
      - "git+https://gitlab.ethz.ch/ansible-let/master_playbook.git"
    creates: /root/.ansible/collections/ansible_collections/let/master_playbook
  when: ansible_master_force_update is undefined
 
- name: "Force update of all ansible collections"
  ansible.builtin.command:
    argv:
      - /usr/local/bin/ansible-galaxy
      - collection
      - install
      - "--force-with-deps"
      - "git+https://gitlab.ethz.ch/ansible-let/master_playbook.git"
  when: ansible_master_force_update is defined

# User configuration
####################
- name: Determine current shell
  ansible.builtin.set_fact:
    shell: "{{ ansible_env.SHELL.split('/')|last }}"
    user: "{{ ansible_env.USER }}"
    home: "{{ ansible_env.HOME }}"

- name: Set environment for bash
  ansible.builtin.set_fact:
    profile: ".bash_profile"
  when: shell == "bash"

- name: Set environment for zsh
  ansible.builtin.set_fact:
    profile: ".zshrc"
  when: shell == "zsh"

- name: Set env variable for Ansible SSH key
  ansible.builtin.lineinfile:
    create: true
    path: "{{ home }}/{{profile}}"
    line: "export ANSIBLE_PRIVATE_KEY_FILE={{ home }}/.ssh/ansible_id_rsa"
    regexp: "^export ANSIBLE_PRIVATE_KEY_FILE"

- name: Set env variable for Ansible vault password file
  ansible.builtin.lineinfile:
    create: true
    path: "{{ home }}/{{ profile }}"
    line: "export ANSIBLE_VAULT_PASSWORD_FILE={{ home }}/.ansible/vault_let.txt"
    regexp: "^export ANSIBLE_VAULT_PASSWORD_FILE"

- name: Set env variable for SnipeIT key file
  ansible.builtin.lineinfile:
    path: "{{ home }}/{{ profile }}"
    line: 'export SNIPEIT_API_KEY_FILE={{ home }}/.ansible/snipeit_key'
    regexp: "^export SNIPEIT_API_KEY_FILE"

- name: Ensure Ansible vault password is installed
  ansible.builtin.copy:
    src: vault_let.txt
    dest: "{{ home }}/.ansible/vault_let.txt"
    owner: "{{ user }}"
    mode: "0600"

- name: Ensure SnipeIT API key is installed
  ansible.builtin.copy:
    src: snipeit_k8s_key
    dest: "{{ home }}/.ansible/snipeit_key"
    owner: "{{ user }}"
    mode: "0600"

# SSH access to managed systems
###############################
- name: Install SSH private key to manage systems
  ansible.builtin.copy:
    src: ansible_private_sshkey
    dest: "{{ home }}/.ssh/ansible_id_rsa"
    owner: "{{ user }}"
    mode: "0600"

# Playbook directory
####################
- name: Ensure playbook directory exists
  file:
    path: "{{ ansible_master_playbook_root }}"
    state: directory
    owner: "{{ user }}"
    mode: "0644"

- name: Link playbook components
  file:
    state: link
    src: "{{ home }}/.ansible/collections/ansible_collections/let/master_playbook/{{ item }}"
    dest: "{{ ansible_master_playbook_root }}/{{ item }}"
  loop:
    - playbooks
    - inventory.yml
    - ansible.cfg
    - site.yml
